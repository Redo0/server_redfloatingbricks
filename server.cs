
package redFloat{
	function fxDtsBrick::plant(%brick){
		%brick.isPlanted = 1;
		%error = parent::plant(%brick);
		
		if(%error==2) %error = 0;
		// if(%error==5) %error = 0;
		
		%brick.isBaseplate=1;
		%brick.willCauseChainKill();
		
		return %error;
	}
};
activatePackage(redFloat);
